package lesson14;

import java.util.Arrays;

public class HomeWork {
    public static void main(String[] args) {
        System.out.println("less7");
        System.out.println(hw_7(new int[]{1, 1, 1, 2, 1}));
        System.out.println(hw_7(new int[]{2, 2, 2, 1, 2, 2, 10, 1}));
        System.out.println(hw_7(new int[]{1, 1, 1, 1, 1}));
        System.out.println(hw_7(new int[]{2, 2, 2, 1, 2, 2, 9, 1}));
        System.out.println("less8");
        System.out.println(Arrays.toString(hw_8(new int[]{1, 2, 3, 8, 5}, 2)));
    }

    static boolean hw_7(int[] myArray) {
        int sumLeft = 0;
        int sumRight = 0;
        int centerLeft = 0;
        int centerRight = myArray.length;
        for (int i = 0; i < myArray.length; i++) {

            if (sumLeft <= sumRight) {
                sumLeft += myArray[centerLeft];
                centerLeft = centerLeft + 1;
            }

            if (sumLeft > sumRight && centerLeft < centerRight) {
                centerRight = centerRight - 1;
                sumRight += myArray[centerRight];
            }

            if (centerLeft >= centerRight) {
                break;
            }

        }
        return sumRight == sumLeft;
    }

    static int[] hw_8(int[] myArray, int number) {
        for (int i = 0; i < number; i++) {
            int previus = 0;
            for (int j = 0; j < myArray.length; j++) {
                int currentValue = myArray[j];
                if (j == 0) {
                    previus = currentValue;
                }else if(j == myArray.length-1) {
                    myArray[j] = previus;
                    myArray[0] = currentValue;
                }else {
                    myArray[j] = previus;
                    previus = currentValue;
                }
            }
        }
        return myArray;
    }
}
