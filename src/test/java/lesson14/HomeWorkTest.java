package lesson14;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

class HomeWorkTest {
    private static final Logger logger = LogManager.getLogger("hw7");
    private static final Logger logger2 = LogManager.getLogger("hw8");

    private static Stream<Arguments> hw_7_complete() {
        List<Arguments> out = new ArrayList<>();
        out.add(Arguments.arguments(new int[]{1, 1, 1, 2, 1}));
        out.add(Arguments.arguments(new int[]{2, 2, 2, 1, 2, 2, 10, 1}));
        return out.stream();

    }

    private static Stream<Arguments> hw_7_error() {
        List<Arguments> out = new ArrayList<>();
        out.add(Arguments.arguments(new int[]{1, 1, 1, 1, 1}));
        out.add(Arguments.arguments(new int[]{2, 2, 2, 1, 2, 2, 9, 1}));
        return out.stream();

    }

    @DisplayName("Тест метода hw_7 верного")
    @ParameterizedTest
    @MethodSource("hw_7_complete")
     void test_hw_7_complete(int[] myArray) {
        logger.info("Старт теста метода test_hw_7_complete"+ Arrays.toString(myArray));
        HomeWork homeWork = new HomeWork();
        Assertions.assertTrue(homeWork.hw_7(myArray));
        logger.info("Завершение теста метода test_hw_7_complete"+Arrays.toString(myArray));
    }

    @DisplayName("Тест метода hw_7 неверного")
    @ParameterizedTest
    @MethodSource("hw_7_error")
    void test_hw_7_error(int[] myArray) {
        logger.info("Старт теста метода test_hw_7_error"+Arrays.toString(myArray));
        HomeWork homeWork = new HomeWork();
        Assertions.assertFalse(homeWork.hw_7(myArray));
        logger.info("Завершение теста метода test_hw_7_error"+Arrays.toString(myArray));
    }


    @DisplayName("Тест метода hw_8")
    @Test
    void test_hw_8() {
        logger2.info("Старт теста метода test_hw_8");
        int[] init = {1, 2, 3, 8, 5};
        HomeWork homeWork = new HomeWork();
        Assertions.assertArrayEquals(new int[]{8, 5, 1, 2, 3},homeWork.hw_8(init,2));
        Assertions.assertArrayEquals(new int[0],homeWork.hw_8(new int[0],2));
        logger2.info("Завершение теста метода test_hw_8");
    }
}
